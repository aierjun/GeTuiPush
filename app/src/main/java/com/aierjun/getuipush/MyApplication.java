package com.aierjun.getuipush;

import android.app.Application;
import android.util.Log;

import com.igexin.sdk.PushManager;

/**
 * Created by aierjun on 2017/11/4.
 */

public class MyApplication extends Application {
    @Override
    public void onCreate() {
        // com.getui.demo.DemoIntentService 为第三方自定义的推送服务事件接收类
        Log.d("Ani" ,"onCreate()");
        PushManager.getInstance().initialize(this, GeTuiPushService.class);

        PushManager.getInstance().registerPushIntentService(this, com.aierjun.getuipush.PushIntentService.class);

        super.onCreate();
    }
}
