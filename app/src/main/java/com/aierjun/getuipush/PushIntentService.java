package com.aierjun.getuipush;

import android.content.Context;
import android.util.Log;

import com.igexin.sdk.GTIntentService;
import com.igexin.sdk.message.GTCmdMessage;
import com.igexin.sdk.message.GTTransmitMessage;

/**
 * Created by aierjun on 2017/11/4.
 */

public class PushIntentService extends GTIntentService {

    public PushIntentService() {

    }

    @Override
    public void onReceiveServicePid(Context context, int pid) {
        Log.d("Ani",pid +"........");
    }

    @Override
    public void onReceiveMessageData(Context context, GTTransmitMessage msg) {
        String s= new String(msg.getPayload());
        Log.d("Ani",msg.getMessageId()+"....touchuan...." + msg.getPayloadId() +"....touchuan...."+ msg.getTaskId() +"....touchuan...." + s);
    }

    @Override
    public void onReceiveClientId(Context context, String clientid) {
        Log.e("Ani", "onReceiveClientId -> " + "clientid = " + clientid);
    }

    @Override
    public void onReceiveOnlineState(Context context, boolean online) {
    }

    @Override
    public void onReceiveCommandResult(Context context, GTCmdMessage cmdMessage) {
        Log.d("Ani",cmdMessage.getAction() +"........");
    }
}
